from .base import *

DEBUG = True

ALLOWED_HOSTS += ['*']

INSTALLED_APPS += ['debug_toolbar', ]

MIDDLEWARE = ['debug_toolbar.middleware.DebugToolbarMiddleware', ] + MIDDLEWARE

DEBUG_TOOLBAR_CONFIG = {
    'SHOW_TOOLBAR_CALLBACK': lambda request: DEBUG
}