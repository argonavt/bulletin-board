import graphene

import bbord.schema


class Query(bbord.schema.Query, graphene.ObjectType):
    pass

schema = graphene.Schema(query=Query)