from modeltranslation.translator import register, TranslationOptions

from bbord.models import Post, Category


@register(Post)
class PostTranslationOptions(TranslationOptions):
    fields = ('title', 'description',)


@register(Category)
class CategoryTranslationOptions(TranslationOptions):
    fields = ('name', 'description',)