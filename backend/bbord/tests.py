from django.utils import translation
from graphene.test import Client
from snapshottest.django import TestCase

from schema import schema
from .models import Category, Post


class CategoryTestCase(TestCase):

    def setUp(self):
        services = Category(
            name="Services",
            slug="services",
            name_fr="Services",
            name_ru="Услуги",
        )
        services.save()

        haircut = Category(
            name="Haircut",
            slug="haircut",
            name_fr="Category",
            name_ru="Стрижка",
            parent=services
        )
        haircut.save()

    def test_obj_to_str(self):
        services = Category.objects.get(name="Services")
        self.assertEqual(str(services), 'Services')

    def test_parent_to_children_relations(self):
        haircut = Category.objects.get(name="Haircut")
        self.assertEqual(haircut.parent.name, 'Services')

    def test_translations(self):
        haircut = Category.objects.get(name="Haircut")
        translation.activate("ru")
        self.assertEqual(haircut.name, "Стрижка")
        translation.activate("en")


class PostTestCase(TestCase):

    def setUp(self):
        category = Category(name="Buy & Sell")
        category.save()

        post = Post(
            title="Laptop",
            title_fr="Laptop",
            title_ru="Ноутбук",
            category=category
        )
        post.save()

    def test_obj_to_str(self):
        post = Post.objects.get(title="Laptop")
        self.assertEqual(str(post), 'Laptop')

    def test_translations(self):
        laptop = Post.objects.get(title="Laptop")
        translation.activate("ru")
        self.assertEqual(laptop.title, "Ноутбук")
        translation.activate("en")


class CategoryAPITestCase(TestCase):

    def setUp(self):
        services = Category(
            name="Services",
            slug="services",
            name_fr="Services",
            name_ru="Услуги",
        )
        services.save()

        haircut = Category(
            name="Haircut",
            slug="haircut",
            name_fr="Category",
            name_ru="Стрижка",
            parent=services
        )
        haircut.save()

    def test_get_category_by_name(self):
        client = Client(schema)
        response = client.execute('''
            {
                category(name: "Haircut"){
                    name
                    slug
                    parent{
                        name
                    }
                }
            }
        ''')
        self.assertMatchSnapshot(response)

    def test_get_category_by_id(self):
        client = Client(schema)

        response = client.execute('''
            {
                category(id: 0){
                    name
                    slug
                    parent{
                        name
                    }
                }
            }
        ''')
        self.assertMatchSnapshot(response)

    def test_category_without_parameters(self):
        client = Client(schema)

        response = client.execute('''
            {
                category{
                    name
                }
            }
        ''')
        self.assertMatchSnapshot(response)

    def test_all_categories(self):
        client = Client(schema)

        response = client.execute('''
            {
                categories{
                    name
                }
            }
        ''')
        self.assertMatchSnapshot(response)

    def test_categories_by_level(self):
        client = Client(schema)

        response = client.execute('''
            {
                categories(level: 0){
                    name
                }
            }
        ''')
        self.assertMatchSnapshot(response)


class PostAPITestCase(TestCase):

    def setUp(self):
        category = Category(
            name="Buy & Sell",
            slug="buy-sell",
        )
        category.save()

        post = Post(
            title="Laptop",
            title_fr="Laptop",
            title_ru="Ноутбук",
            description="test",
            category=category
        )
        post.save()

    def test_all_posts(self):
        client = Client(schema)

        response = client.execute('''
            {
                posts{
                    title
                    description
                    category{
                        name
                    }
                }
            }
        ''')
        self.assertMatchSnapshot(response)

    def test_posts_by_category(self):
        client = Client(schema)

        response = client.execute('''
            {
                posts(categorySlug: "buy-sell"){
                    title
                    description
                    category{
                        name
                    }
                }
            }
        ''')
        self.assertMatchSnapshot(response)

    def test_post_by_name(self):
        client = Client(schema)

        response = client.execute('''
            {
                post(title: "Laptop"){
                    title
                    description
                    category{
                        name
                    }
                }
            }
        ''')
        self.assertMatchSnapshot(response)

    def test_post_by_id(self):
        client = Client(schema)

        response = client.execute('''
            {
                post(id: 0){
                    title
                    description
                    category{
                        name
                    }
                }
            }
        ''')
        self.assertMatchSnapshot(response)

    def test_post_without_parameters(self):
        client = Client(schema)

        response = client.execute('''
            {
                post{
                    title
                    description
                    category{
                        name
                    }
                }
            }
        ''')
        self.assertMatchSnapshot(response)

