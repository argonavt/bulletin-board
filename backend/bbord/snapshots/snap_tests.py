# -*- coding: utf-8 -*-
# snapshottest: v1 - https://goo.gl/zC4yUc
from __future__ import unicode_literals

from snapshottest import Snapshot

snapshots = Snapshot()

snapshots['CategoryAPITestCase::test_all_categories 1'] = {
    'data': {
        'categories': [
            {
                'name': 'Services'
            },
            {
                'name': 'Haircut'
            }
        ]
    }
}

snapshots['CategoryAPITestCase::test_categories_by_level 1'] = {
    'data': {
        'categories': [
            {
                'name': 'Services'
            }
        ]
    }
}

snapshots['CategoryAPITestCase::test_category_without_parameters 1'] = {
    'data': {
        'category': None
    }
}

snapshots['CategoryAPITestCase::test_get_category_by_id 1'] = {
    'data': {
        'category': None
    },
    'errors': [
        {
            'locations': [
                {
                    'column': 17,
                    'line': 3
                }
            ],
            'message': 'Category matching query does not exist.',
            'path': [
                'category'
            ]
        }
    ]
}

snapshots['CategoryAPITestCase::test_get_category_by_name 1'] = {
    'data': {
        'category': {
            'name': 'Haircut',
            'parent': {
                'name': 'Services'
            },
            'slug': 'haircut'
        }
    }
}

snapshots['PostAPITestCase::test_all_posts 1'] = {
    'data': {
        'posts': [
            {
                'category': {
                    'name': 'Buy & Sell'
                },
                'description': 'test',
                'title': 'Laptop'
            }
        ]
    }
}

snapshots['PostAPITestCase::test_post_by_id 1'] = {
    'data': {
        'post': None
    },
    'errors': [
        {
            'locations': [
                {
                    'column': 17,
                    'line': 3
                }
            ],
            'message': 'Post matching query does not exist.',
            'path': [
                'post'
            ]
        }
    ]
}

snapshots['PostAPITestCase::test_post_by_name 1'] = {
    'data': {
        'post': {
            'category': {
                'name': 'Buy & Sell'
            },
            'description': 'test',
            'title': 'Laptop'
        }
    }
}

snapshots['PostAPITestCase::test_post_without_parameters 1'] = {
    'data': {
        'post': None
    }
}

snapshots['PostAPITestCase::test_posts_by_category 1'] = {
    'data': {
        'posts': [
            {
                'category': {
                    'name': 'Buy & Sell'
                },
                'description': 'test',
                'title': 'Laptop'
            }
        ]
    }
}
