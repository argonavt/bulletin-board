import graphene
from graphene_django.types import DjangoObjectType

from .models import Category, Post


class CategoryType(DjangoObjectType):
    class Meta:
        model = Category


class PostType(DjangoObjectType):
    class Meta:
        model = Post


class Query(object):
    category = graphene.Field(CategoryType, id=graphene.Int(), name=graphene.String())
    categories = graphene.List(CategoryType, level=graphene.Int())
    post = graphene.Field(PostType, id=graphene.Int(), title=graphene.String())
    posts = graphene.List(PostType, category_slug=graphene.String())

    def resolve_categories(self, info, level=None, **kwargs):

        if level is not None and level >= 0:
            return Category.objects.filter(level=level)

        return Category.objects.all()

    def resolve_posts(self, info, **kwargs):
        category_slug = kwargs.get('category_slug')

        if category_slug is not None:
            return Post.objects.select_related('category').filter(category__slug=category_slug)

        return Post.objects.select_related('category').all()

    def resolve_category(self, info, **kwargs):
        id = kwargs.get('id')
        name = kwargs.get('name')

        if id is not None:
            return Category.objects.get(pk=id)

        if name is not None:
            return Category.objects.get(name=name)

        return None

    def resolve_post(self, info, **kwargs):
        id = kwargs.get('id')
        title = kwargs.get('title')

        if id is not None:
            return Post.objects.get(pk=id)

        if title is not None:
            return Post.objects.get(title=title)

        return None
