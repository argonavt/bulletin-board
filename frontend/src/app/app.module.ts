import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ApolloBoost, ApolloBoostModule } from 'apollo-angular-boost';
import { RouterModule } from '@angular/router';
import { appRoutes } from './routes';
import { AppComponent } from './app.component';
import { NavigationModule } from './navigation/navigation.module';
import { PostsModule } from './posts/posts.module';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports: [
        BrowserModule,
        HttpClientModule,
        ApolloBoostModule,
        NavigationModule,
        PostsModule,
        RouterModule.forRoot(appRoutes)
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {
    constructor(boost: ApolloBoost) {
        boost.create({
            uri: '/graphql/'
        });
    }
}
