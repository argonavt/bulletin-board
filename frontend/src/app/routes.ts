import { Routes } from '@angular/router';
import { PostListComponent } from './posts/post-list.component';

export const appRoutes: Routes = [
    {
        path: 'category/:slug',
        component: PostListComponent
    },
];
