import { NgModule } from '@angular/core';
import { MenubarModule } from 'primeng/menubar';
import { NavigationComponent } from './navigation.component';

@NgModule({
    imports: [
        MenubarModule
    ],
    declarations: [
        NavigationComponent
    ],
    exports: [
        NavigationComponent
    ]
})
export class NavigationModule {
}
