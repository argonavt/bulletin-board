import { DocumentNode, gql } from 'apollo-angular-boost';


const fragments: {
    [key: string]: DocumentNode
} = {
    CategoryFields: gql`
        fragment CategoryFields on CategoryType {
            name
            slug
        }
    `,
    CategoryRecursive: gql`
        fragment CategoryRecursive on CategoryType {
            ...CategoryFields
            children {
                ...CategoryFields
                children {
                    ...CategoryFields
                    children {
                        ...CategoryFields
                    }
                }
            }
        }
  `,
};

export function getCategories(level: Number = -1): DocumentNode {
    return gql`
        {
            categories(level: ${level}){
                ...CategoryRecursive
            }
        }
        ${fragments['CategoryRecursive']}
        ${fragments['CategoryFields']}
    `;
}
