import { Component, OnInit } from '@angular/core';
import { Apollo } from 'apollo-angular-boost';
import { MenuItem } from 'primeng/api';
import { getCategories } from './query';

@Component({
    selector: 'app-navigation',
    template: `
        <p-menubar [model]="categories"></p-menubar>
    `,
    styles: []
})
export class NavigationComponent implements OnInit {
    categories: MenuItem[] = [];

    constructor(private apollo: Apollo) {}

    ngOnInit() {
        this.apollo
            .watchQuery({query: getCategories(0)})
            .valueChanges
            .subscribe(({data}) => this.categories.push(...this.generateTree(data['categories'])));
    }

    /**
     * Transform graphql data to primeng MenuItem object
     */
    generateTree(categories: any[]): MenuItem[] {
        return categories.map(category => {
            const subTree = {
                label: category.name,
                routerLink: ['category', category.slug]
            };
            const items = this.generateTree(category.children);
            if (items.length) {
                subTree['items'] = items;
            }
            return subTree;
        });
    }
}
