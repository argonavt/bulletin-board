import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { switchMap } from 'rxjs/operators';
import { Apollo } from 'apollo-angular-boost';
import { getPosts } from './query';
import { Post } from '../../models/post';

@Component({
    selector: 'app-post-list',
    template: `
        <div *ngFor="let post of posts" class="row">
            <div class="col-12">
                <h2>{{ post.title }}</h2>
                <div>{{ post.description }}</div>
            </div>
        </div>
    `
})
export class PostListComponent implements OnInit {
    posts: Post[];

    constructor(
        private apollo: Apollo,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.route.paramMap.pipe(
            switchMap((params: ParamMap) => {
                const slug = params.get('slug');
                return this.apollo.watchQuery({query: getPosts(slug)}).valueChanges;
            })
        ).subscribe(result => this.posts = result.data['posts']);
    }
}
