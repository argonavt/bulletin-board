import { DocumentNode, gql } from 'apollo-angular-boost';


export function getPosts(categorySlug: string): DocumentNode {
    return gql`
        {
            posts(categorySlug: "${categorySlug}"){
                title
                description
                createTime
            }
        }
    `;
}
