import { Category } from './category';

export class Post {
  title: string;
  category: Category;
  description: string;
  create_time: string;
}
