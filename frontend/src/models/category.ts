export class Category {
  name: string;
  slug: string;
  parent: Category;
  image: string;
  description: string;
}
